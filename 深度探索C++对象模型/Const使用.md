#### Const使用

##### 函数传参：

非内置类型T的函数入参，声明为const T&；

```c++
void Fun(const T& param);
```

理由：内置类型的变量，如int，在作为函数形式入参时，默认是以值传递进行，即形参值改变不会影响实参，但是对于自定义类类型的函数入参，为了减少对象的构造和析构，通常会将入参类型设置为引用或者指针类型，这样就导致形参的改变会影响实参，所以，在不需要进行原值修改的只读入参，声明为const引用，可以强制编译器检查不恰当赋值的发生，也和内置类型的行为更接近；

##### 函数返回值：

函数返回值尽量声明为const；

```c++
const T Fun();
```

理由：以下语句是合法的：

```c++
class A
{
    public:
    	A(int nParam):m_nMem(nParam){}
    	int m_nMen;
};

A GetA(int n)
{
    return A(n);
}

const A GetCA(int n)
{
    return A(n);
}

CString GetStr(const char* pszTmp)
{
    return CString(pszTmp);
}

int main()
{
    GetA(4) = 5;	/// 正确
    GetCA(4) = 5; /// 错误
    
    
    /// 特别地，以下条件成立
    if (GetStr("ABC") = "DEF")
    {
        ///
    }
    
    return 0;
}
```

